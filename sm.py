from __future__ import unicode_literals

from chatterbot.trainers import ListTrainer
from chatterbot import ChatBot
import os
import sys
import importlib
from collections import Counter
from chatterbot import utils
from chatterbot.logic import LogicAdapter
from chatterbot.filters import Filter
from datetime import datetime

bot = ChatBot('sandy',
              storage_adapter='chatterbot.storage.SQLStorageAdapter',
              filters=["chatterbot.filters.RepetitiveResponseFilter"],
              preprocessors=[
                  'chatterbot.preprocessors.clean_whitespace',
                  'chatterbot.preprocessors.convert_to_ascii'
              ],
     logic_adapters=[
        {    'import_path': 'chatterbot.logic.MathematicalEvaluation'

             },
        {
            "import_path": "chatterbot.logic.BestMatch",
            "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
            "response_selection_method": "chatterbot.response_selection.get_first_response"
        },
        { 'import_path': 'chatterbot.logic.LowConfidenceAdapter',
            'threshold': 0.20,
            'default_response': 'I am sorry could not understand you.'

          },
        ],
              input_adapter="chatterbot.input.VariableInputTypeAdapter",
              database="datsrsd"
        )
bot.set_trainer(ListTrainer)
for _file in os.listdir('E:/Chatbot_Project-master/data/'):
   chats = open('E:/Chatbot_Project-master/data/'+_file,encoding='latin-1').readlines()
   bot.train(chats)
hello=input("Give your proper information")
print('Welcome to Stock World '    +hello+'\n')

while True:
 request = input('You: ')
 if request.strip() == 'Bye' or request.strip() == 'bye':
     print('Stock World:bye')
     break;

 response = bot.get_response(request)
 print('Stock World:', response)


class MyFilter(Filter):

    def filter_selection(self, chatterbot):
        return datsrs.query
