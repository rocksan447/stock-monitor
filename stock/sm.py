from chatterbot.trainers import ListTrainer
from chatterbot.trainers import ChatterBotCorpusTrainer
from chatterbot import ChatBot
import os
from chatterbot.logic import LogicAdapter

bot = ChatBot('sandy',
              storage_adapter='chatterbot.storage.SQLStorageAdapter',
     logic_adapters=[
        {
            "import_path": "chatterbot.logic.BestMatch",
        },
        {
            'import_path': 'chatterbot.logic.LowConfidenceAdapter',
            'threshold': 0.70,
            'default_response': 'I am sorry, but I do not understand.'
        }
        ],
        )
        #trainer='chatterbot.trainers.ListTrainers')
#bot.set_trainer(ListTrainer)
bot.set_trainer(ChatterBotCorpusTrainer)
for _file in os.listdir('E:/Chatbot_Project-master/data/'):
   chats = open('E:/Chatbot_Project-master/data/'+_file,encoding='latin-1').readlines()
  # bot.train(chats)
   bot.train(
    "chatterbot.corpus.english")

while True:
 request = input('You: ')
 response =bot.get_response(request)
 print('User:',response)
